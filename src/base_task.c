/* based_task.c -- A basic real-time task skeleton. 
 *
 * This (by itself useless) task demos how to setup a 
 * single-threaded LITMUS^RT real-time task.
 */

/* First, we include standard headers.
 * Generally speaking, a LITMUS^RT real-time task can perform any
 * system call, etc., but no real-time guarantees can be made if a
 * system call blocks. To be on the safe side, only use I/O for debugging
 * purposes and from non-real-time sections.
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/* Second, we include the LITMUS^RT user space library header.
 * This header, part of liblitmus, provides the user space API of
 * LITMUS^RT.
 */
#include <litmus.h>
#include <libavcodec/avcodec.h>
#include <libavformat/avformat.h>
#include <libavutil/avconfig.h>
#include <libswscale/swscale.h>
#include <libavutil/imgutils.h>
#include <SDL2/include/SDL.h>
#include <SDL2/include/SDL_thread.h>

/* Next, we define period and execution cost to be constant. 
 * These are only constants for convenience in this example, they can be
 * determined at run time, e.g., from command line parameters.
 *
 * These are in milliseconds.
 */
#define PERIOD     34
#define RELATIVE_DEADLINE 34
#define EXEC_COST         20

/* Catch errors.
 */
#define CALL( exp ) do { \
		int ret; \
		ret = exp; \
		if (ret != 0) \
			fprintf(stderr, "%s failed: %m\n", #exp);\
		else \
			fprintf(stderr, "%s ok.\n", #exp); \
	} while (0)


/* Declare the periodically invoked job. 
 * Returns 1 -> task should exit.
 *         0 -> task should continue.
 */
int job(void);

void cleanup(void);

void cleanupFFmpeg(void);

void cleanupSDL(void);

void setupDecoder(char filePath[]);

void setupDisplay(void);


AVFormatContext *pFormatCtx = NULL;
AVCodecContext *pCodecCtx = NULL;
AVCodec *pCodec = NULL;
int i, videoStream= -1;
SDL_Event event;
SDL_Window *screen;
SDL_Renderer *renderer;
SDL_Texture *texture;
Uint8 *yPlane, *uPlane, *vPlane;
size_t yPlaneSz, uvPlaneSz;
int uvPitch;
struct SwsContext *swsCtx = NULL;
int frameFinished;
AVPacket packet;
AVFrame *pFrame;
AVFrame picture;
SDL_Event sdlEvent;

/* typically, main() does a couple of things: 
 * 	1) parse command line parameters, etc.
 *	2) Setup work environment.
 *	3) Setup real-time parameters.
 *	4) Transition to real-time mode.
 *	5) Invoke periodic or sporadic jobs.
 *	6) Transition to background mode.
 *	7) Clean up and exit.
 *
 * The following main() function provides the basic skeleton of a single-threaded
 * LITMUS^RT real-time task. In a real program, all the return values should be 
 * checked for errors.
 */
int main(int argc, char** argv)
{
	int do_exit;
	struct rt_task param;
	char filePath[] = "/home/nikhil/Downloads/video1.mp4";

	/* Setup task parameters */
	init_rt_task_param(&param);
	param.exec_cost = ms2ns(EXEC_COST);
	param.period = ms2ns(PERIOD);
	param.relative_deadline = ms2ns(RELATIVE_DEADLINE);

	/* What to do in the case of budget overruns? */
	param.budget_policy = NO_ENFORCEMENT;

	/* The task class parameter is ignored by most plugins. */
	param.cls = RT_CLASS_SOFT;

	/* The priority parameter is only used by fixed-priority plugins. */
	param.priority = LITMUS_LOWEST_PRIORITY;

	/* The task is in background mode upon startup. */


	/*****
	 * 1) Command line paramter parsing would be done here.
	 */



	/*****
	 * 2) Work environment (e.g., global data structures, file data, etc.) would
	 *    be setup here.
	 */



	/*****
	 * 3) Setup real-time parameters. 
	 *    In this example, we create a sporadic task that does not specify a 
	 *    target partition (and thus is intended to run under global scheduling). 
	 *    If this were to execute under a partitioned scheduler, it would be assigned
	 *    to the first partition (since partitioning is performed offline).
	 */
	CALL( init_litmus() );

	/* To specify a partition, do
	 *
	 * param.cpu = CPU;
	 * be_migrate_to(CPU);
	 *
	 * where CPU ranges from 0 to "Number of CPUs" - 1 before calling
	 * set_rt_task_param().
	 */
	CALL( set_rt_task_param(gettid(), &param) );


	/*****
	 * 4) Transition to real-time mode.
	 */
	CALL( task_mode(LITMUS_RT_TASK) );

	/* The task is now executing as a real-time task if the call didn't fail. 
	 */

	setupDecoder(filePath);
	
	setupDisplay();

	/*****
	 * 5) Invoke real-time jobs.
	 */
	do {
		/* Wait until the next job is released. */
		sleep_next_period();
		/* Invoke job. */
		do_exit = job();		
	} while (!do_exit);


	
	/*****
	 * 6) Transition to background mode.
	 */
	CALL( task_mode(BACKGROUND_TASK) );


	/***** 
	 * 7) Clean up, maybe print results and stats, and exit.
	 */
	cleanup();
	return 0;
}

/*
* 1) Initialize FFmpeg variables
* 2) Open the video file and get the stream info
* 3) Determine the decoder required
* 4) Open the appropriate codec
*/
void setupDecoder(char filePath[]) {
	av_register_all();
	
	fprintf(stderr, "\nOpen video file at the location: %s", filePath);
	//Open the video file and set the context
	if(avformat_open_input(&pFormatCtx, filePath, NULL, NULL) != 0){
		fprintf(stderr,"\nUnable to open the video file from the path: %s",filePath);		
		exit(1);	
	}

	fprintf(stderr, "\nSuccessfully openend the video file at location: %s", filePath);

	//Get stream info
	if(avformat_find_stream_info(pFormatCtx, NULL) <0) {
		fprintf(stderr,"\nUnable to get the stream info");
		exit(1);	
	}

	fprintf(stderr, "\nObtained stream info");	

	//Find and use the first stream
	for(i=0; i<pFormatCtx->nb_streams; i++)
		if(pFormatCtx->streams[i]->codec->codec_type==AVMEDIA_TYPE_VIDEO) {
			videoStream=i;
			break;
		}
	if(videoStream == -1) {
		fprintf(stderr,"\nUnable to find the video stream in the given stream");
		exit(1);
	}
	
	//Get pointer to the codec context for video stream
	pCodecCtx = pFormatCtx->streams[videoStream]->codec;
	
	//Find the decoder for the video stream
	pCodec = avcodec_find_decoder(pCodecCtx->codec_id);
	if(pCodec == NULL) {
	  	fprintf(stderr,"\nUnable to decode. Unsupported codec");
	  	exit(1);		
	}

	//Is this block really needed? Check!
/*
	//Copy context
	pCodecCtx = avcodec_alloc_context3(pCodec);
	if(avcodec_copy_context(pCodecCtx, pCodecCtxOrig) != 0) {
	 //Log error. Cannot copy codec
	 fprintf(stderr,"\nUnable to copy the codec");
       	 fflush(stderr);
         return -1;
	}
*/
	//Open codec
	if(avcodec_open2(pCodecCtx, pCodec, NULL) < 0) {
	  	fprintf(stderr,"\nUnable to open the codec");  
          	exit(1);	
	}
}


/*
* 1) Initialize SDL
* 2) Create a display
* 3) Create a renderer
* 4) Initialize frame
*/
void setupDisplay() {
	//Initialize SDL
	if(SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO | SDL_INIT_TIMER)) {
	  fprintf(stderr, "\nCould not initialize SDL - %s\n", SDL_GetError());
	  exit(1);
	}

	//Create a display
	screen = SDL_CreateWindow("VideoApp - LITMUS-RT", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED,pCodecCtx->width, pCodecCtx->height, 0);

	if(!screen) {
	  fprintf(stderr, "\nSDL: failed to set video mode. Exiting");
	  exit(1);
	}

	//Create a renderer
    	renderer = SDL_CreateRenderer(screen, -1, 0);
    	if (!renderer) {
          fprintf(stderr, "SDL: could not create renderer - exiting\n");
          exit(1);
    	}
        
	
	texture = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_YV12, SDL_TEXTUREACCESS_STREAMING, pCodecCtx->width, pCodecCtx->height);
	if (!texture) {
          fprintf(stderr, "SDL: could not create texture - exiting\n");
          exit(1);
    	}
	

	//Initialize SWS context
	swsCtx = sws_getContext(pCodecCtx->width, pCodecCtx->height, pCodecCtx->pix_fmt, pCodecCtx->width, pCodecCtx->height, AV_PIX_FMT_YUV420P, SWS_BILINEAR, NULL, NULL, NULL);

        // set up YV12 pixel array (12 bits per pixel)
    	yPlaneSz = pCodecCtx->width * pCodecCtx->height;
    	uvPlaneSz = pCodecCtx->width * pCodecCtx->height / 4;
    	yPlane = (Uint8*)malloc(yPlaneSz);
    	uPlane = (Uint8*)malloc(uvPlaneSz);
    	vPlane = (Uint8*)malloc(uvPlaneSz);
 
   	if (!yPlane || !uPlane || !vPlane) {
          fprintf(stderr, "Could not allocate pixel buffers - exiting\n");
          exit(1);
    	} 

    	uvPitch = pCodecCtx->width / 2;
	i=0;
	pFrame = av_frame_alloc();
}

int job(void) 
{


/*
	//Determine the buffer size and allocate buffer
	numBytes = av_image_get_buffer_size(AV_PIX_FMT_RGB24, pCodecCtx->width, pCodecCtx->height);
	buffer = (uint8_t *) av_malloc(numBytes*sizeof(uint8_t));
	av_image_fill_arrays(buffer, numBytes, (AVPicture *)pFrameRGB, AV_PIX_FMT_RGB24, pCodecCtx->width, pCodecCtx->height,0);
*/
	

	if(av_read_frame(pFormatCtx, &packet) >= 0) {
	   //Check if the packet is from the video stream
	   if(packet.stream_index == videoStream) {
   	      //Decode video frame
	      avcodec_decode_video2(pCodecCtx, pFrame, &frameFinished, &packet);
	      
	      //Was the frame read
	      if(frameFinished) {

		picture.data[0] = yPlane;
		picture.data[1] = uPlane;
		picture.data[2] = vPlane;

		picture.linesize[0] = pCodecCtx->width;
		picture.linesize[1] = uvPitch;
		picture.linesize[2] = uvPitch;

		//Convert the image into YUV format for SDL to use
		sws_scale(swsCtx, (uint8_t const * const *)pFrame->data, pFrame->linesize, 0, pCodecCtx->height, picture.data, picture.linesize);
		                SDL_UpdateYUVTexture(
                        texture,
                        NULL,
                        yPlane,
                        pCodecCtx->width,
                        uPlane,
                        uvPitch,
                        vPlane,
                        uvPitch
                    );

	       if(SDL_PollEvent(&event)) {
        	        if(event.type == SDL_QUIT){
                	        fprintf(stderr, "\nTerminate call received from SDL screen. Exiting.\n");
                       		exit(1);
                	}   
        	}   

                SDL_RenderClear(renderer);
                SDL_RenderCopy(renderer, texture, NULL, NULL);
                SDL_RenderPresent(renderer);
	      }
	   }
	

	} else {
	  fprintf(stderr, "\nFinished processing the video");
	  return 1;	
	}	
	return 0;
}

void cleanup() {
	cleanupFFmpeg();
	cleanupSDL();
	fprintf(stderr, "\nDone with cleanup task. Exiting\n");	
}

void cleanupFFmpeg() {
	 //Free the memory allocated for the packet
	 av_packet_unref(&packet);

	//Free the memory allocated for the frame
	av_frame_unref(&picture);
	av_frame_unref(pFrame);

	//Close the codec & input
	avcodec_close(pCodecCtx);
	avformat_close_input(&pFormatCtx);	
}

void cleanupSDL() {
	SDL_DestroyTexture(texture);
	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(screen);
	SDL_Quit();	
}
